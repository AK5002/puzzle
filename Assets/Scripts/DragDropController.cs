﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragDropController : MonoBehaviour,IDragHandler,IDropHandler
{
    public event Action<Item> OnDropped;

    public Item selectedObject;
    
    public void OnDrag(PointerEventData eventData)
    {
        if (selectedObject != null && selectedObject.IsActive &&
            eventData.pointerCurrentRaycast.screenPosition != Vector2.zero)
            selectedObject.transform.position = eventData.pointerCurrentRaycast.screenPosition;
    }

    public void OnDrop(PointerEventData eventData)
    {
        if(selectedObject != null && selectedObject.IsActive)
            OnDropped?.Invoke(selectedObject);
    }
}
