﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GamePlayController m_GamePlayController;

    private void Setup()
    {
        m_GamePlayController.OnGameOver += OnGameOver;
    }
    
    private void Awake()
    {
        Setup();
        m_GamePlayController.Setup();
        
    }

    void OnGameOver()
    {
        Debug.Log("GameOver");
        m_GamePlayController.m_UIController.gameObject.SetActive(true);
        
    }
}
