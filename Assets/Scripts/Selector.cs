﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Selector : MonoBehaviour,IPointerDownHandler
{
    public event Action<Item> OnSelect; 
    
    public void OnPointerDown(PointerEventData eventData)
    {
        Item selectedObject = eventData.pointerEnter.GetComponent<Item>();

        if (selectedObject != null)
            OnSelect?.Invoke(selectedObject);
    }
}
