﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.PlayerLoop;
using Random = System.Random;

public class Puzzle : MonoBehaviour
{
    [SerializeField] public int m_PuzzleId;
    [SerializeField] public int m_PuzzleCount;
    [SerializeField] public List<Item> m_ActiveItems;
    [SerializeField] public List<Item> m_PassiveItems;
    [SerializeField] public Transform m_Center;
    [SerializeField] private float m_RadiusMin;
    
    public RectTransform DragDrop;

    private float XRadiusMax;
    private float YRadiusMax;

    public void Setup()
    {
        XRadiusMax = DragDrop.rect.width / 2f - 100f;
        YRadiusMax = DragDrop.rect.height / 2f -100f;
        
        Debug.Log(XRadiusMax);
    }
    
    public void SetupGame(RectTransform dragDropTransform)
    {
        DragDrop = dragDropTransform;
        
        Setup();
        
        Vector2 randomPosition;

        foreach (var puzzleItem in m_ActiveItems)
        {
            puzzleItem.m_RectTransform.anchoredPosition = RandomPosition();
        }
    }

    private Vector2 RandomPosition()
    {
        float xPos = UnityEngine.Random.Range(m_RadiusMin, XRadiusMax + m_RadiusMin + 100f);

        if (xPos > XRadiusMax)
             xPos = m_RadiusMin -  xPos + 100f;
        
        float yPos = UnityEngine.Random.Range(m_RadiusMin, YRadiusMax + m_RadiusMin + 100f);

        if (yPos > YRadiusMax)
            yPos = m_RadiusMin - yPos + 100f;
        
        return new Vector2(xPos,yPos);
    }
}
