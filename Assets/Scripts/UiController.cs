﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UiController : MonoBehaviour,IPointerClickHandler
{
   [SerializeField] public GameObject m_Button;
   
   public event Action OnPressed;

   public void Setup()
   {
      gameObject.SetActive(false);
   }
   
   public void OnPointerClick(PointerEventData eventData)
   {

      if (eventData.pointerEnter == m_Button)
      {
         OnPressed?.Invoke();
      }
   }
}
