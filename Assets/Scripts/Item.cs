﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    [SerializeField] public RectTransform m_RectTransform;
    [SerializeField] public bool IsActive;
    [SerializeField] public int Id;
    
}
