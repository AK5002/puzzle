﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.SocialPlatforms.GameCenter;

public class GamePlayController : MonoBehaviour
{
   public event Action OnGameOver;
   
   [SerializeField] private DragDropController m_DragDrop;
   [SerializeField] private Transform m_PuzzleParent;
   [SerializeField] private Selector m_Selector;
   [SerializeField] private List<Puzzle> m_PuzzlePrefs;
   [SerializeField] public UiController m_UIController;
   public GameObject CurrentPuzzle;
   private int PlacedItems;
   public int Level =0;
   private Puzzle Puzzle;
   private bool firsttime = true;
   public void Setup()
   {
      m_DragDrop.OnDropped += OnDroppedHandler;
      
      m_Selector.OnSelect += OnSelectHandler;
      
      LoadGame();
   }
   private void LoadGame()
   {
      m_UIController.OnPressed -= LoadGame;
      m_UIController.Setup();
      DestroyImmediate(CurrentPuzzle.gameObject);
      PlacedItems = 0;
      Puzzle = m_PuzzlePrefs.Find(puzzle => puzzle.m_PuzzleId == Level);
      CurrentPuzzle = Instantiate(Puzzle,m_PuzzleParent).gameObject;
      
      Level++;
      m_UIController.OnPressed += LoadGame;
   }
   
   private void OnDroppedHandler(Item selectedObject)
   {
      
      Item passiveObject = Puzzle.m_PassiveItems.Find(passive => passive.Id == selectedObject.Id);
      
      if (!IsOverlap(selectedObject.m_RectTransform, passiveObject.m_RectTransform)) return;

      selectedObject.m_RectTransform.anchoredPosition = passiveObject.transform.position;
      selectedObject.IsActive = false;

      PlacedItems++;

      if (PlacedItems == Puzzle.m_PuzzleCount)
         OnGameOver?.Invoke();
   }

   private bool IsOverlap(RectTransform rectTransform1, RectTransform rectTransform2)
   {
      Rect rect1 = new Rect(rectTransform1.localPosition.x,rectTransform1.localPosition.y,rectTransform1.rect.width,rectTransform1.rect.height);
      Rect rect2 = new Rect(rectTransform2.localPosition.x,rectTransform2.localPosition.y,rectTransform2.rect.width,rectTransform2.rect.height);

      return rect1.Overlaps(rect2);
   }
   
   private void OnSelectHandler(Item selectedObject)
   {
      m_DragDrop.selectedObject = selectedObject;
   }
}
